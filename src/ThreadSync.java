/**
 *
 * Justin Brooks
 * CS350 Project #4
 * October 23, 2013
 *
 */

import java.lang.Thread;
import java.util.concurrent.*;

public class ThreadSync
{

    // Semaphores
    public static Semaphore oneAvailible = new Semaphore(8);
    public static Semaphore twoAvailible = new Semaphore(0);
    public static Semaphore fourAvailible = new Semaphore(0);
    public static Semaphore eightAvalible = new Semaphore(0);
    // Manages the patterns
    public static Semaphore master = new Semaphore(0);

    private static boolean runFlag = true;

    public static void main( String[] args ) {

        // create and start each runnable
        Runnable task1 = new TaskPrint1();
        Runnable task2 = new TaskPrint2();
        Runnable task3 = new TaskPrint4();
        Runnable task4 = new TaskPrint8();

        Thread thread1 = new Thread( task1 );
        Thread thread2 = new Thread( task2 );
        Thread thread3 = new Thread( task3 );
        Thread thread4 = new Thread( task4 );

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();

        // Let them run for 500ms
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // put up the stop sign
        runFlag=false;

        thread4.interrupt();
        thread3.interrupt();
        thread2.interrupt();
        thread1.interrupt();

    }

    public static class TaskPrint1 implements Runnable
    {
        public void run(){
            while (runFlag) {
                try {
                    oneAvailible.acquire();
                } catch (InterruptedException ex) {
                    break;
                }
                System.out.printf( "%s\n", "1");
                twoAvailible.release();
                master.release();
            }
        }
    }

    public static class TaskPrint2 implements Runnable
    {
        public void run(){
            while (runFlag) {
                try {
                    twoAvailible.acquire(2);
                    master.acquire(8);
                } catch (InterruptedException ex) {
                    break;
                }
                System.out.printf( "%s\n", "2");
                fourAvailible.release(2);
                master.release(9);
            }
        }
    }

    public static class TaskPrint4 implements Runnable
    {
        public void run(){
            while (runFlag) {
                try {
                    fourAvailible.acquire(4);
                    master.acquire(12);
                } catch (InterruptedException ex) {
                    break;
                }
                System.out.printf( "%s\n", "4");
                eightAvalible.release(4);
                master.release(13);
            }
        }
    }

    public static class TaskPrint8 implements Runnable
    {
        public void run(){
            while (runFlag) {
                try {
                    eightAvalible.acquire(8);
                    master.acquire(14);
                } catch (InterruptedException ex) {
                    break;
                }
                System.out.printf( "%s\n", "8");
                oneAvailible.release(8);
            }
        }
    }
}